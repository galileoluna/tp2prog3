package grafo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class FlujoMaximoTest {

	@Test
	void bfsTest() {
	
	}
	@Test
	void EdKarpTest() {
		int mat[][] = new int[4][4];
		mat[0][1]=10;
		mat[0][2]=12;
		mat[1][0]=4;
		mat[1][3]=14;
		mat[2][1]=9;
		mat[3][2]=7;
		FlujoMaximo flujo=new FlujoMaximo(mat.length);
		Assert.assertEquals(23, flujo.edKarp(mat, 0, mat.length));
	}
}
