package grafo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;




class GrafoTest {
	private Grafo g = new Grafo();
			
	public void Cargas() {
		g.agregarNodo(new NodoGas("Productor", "1", 16));
		g.agregarNodo(new NodoGas("Productor", "2", 13));
		g.agregarNodo(new NodoGas("Consumidor", "3", 20));
		g.agregarNodo(new NodoGas("Consumidor", "4", 4));
		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 12);
		g.agregarArista(1, 0, 4);
		g.agregarArista(1, 3, 14);
		g.agregarArista(2, 1, 9);
		g.agregarArista(3, 2, 7);
		
	}
	

	@Test
	void FlujoMaximoTest() {
		Cargas();
		Assert.assertEquals(g.flujoMax(), 23);
	}

	@Test
	void cantVerticesTest() {
		Cargas();	
		Assert.assertEquals(g.vertices(),4);
	}
	
	@Test
	void agregarNodoTest() {
		Cargas();
		NodoGas nodo= new NodoGas("Productor", "1", 16);
		NodoGas nodofalso= new NodoGas("Productr", "1q", 16);
		Assert.assertTrue(g.existeNodo(nodo));
		Assert.assertFalse(g.existeNodo(nodofalso));
	}
	
	@Test
	void recibirAristaTest() {
		Cargas();
		int mat[][] = new int[4][4];
		mat[0][1]=10;
		mat[0][2]=12;
		mat[1][0]=4;
		mat[1][3]=14;
		mat[2][1]=9;
		mat[3][2]=7;
		
		int mat1[][] = new int[4][4];
		mat1[0][1]=111;
		mat1[0][2]=12;
		mat1[1][0]=4;
		mat1[1][3]=14;
		mat1[2][1]=9;
		mat1[3][2]=7;
	

		Assert.assertArrayEquals(mat, g._capacidadesAristas);
		Assert.assertFalse(Arrays.equals(mat1, g._capacidadesAristas));
		
		
	}
	
	@Test 
	void agregarAristaTest()
	{
		Cargas();	
	}
	
	@Test 
	void eliminarAristaTest()
	{
		Cargas();	
	}
	
	@Test 
	void existeNodoTest()
	{
		Cargas();	
	}
	
	@Test 
	void getAristaTest()
	{
		Cargas();	
	}
	
	
}