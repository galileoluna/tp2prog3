package main;

import grafo.Grafo;
import grafo.NodoGas;

public class main2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Grafo g=new Grafo();
		g.agregarNodo(new NodoGas("Productor", "1", 16));
		g.agregarNodo(new NodoGas("Productor", "2", 13));
		g.agregarNodo(new NodoGas("Consumidor", "3", 20));
		g.agregarNodo(new NodoGas("Consumidor", "4", 4));
		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 12);
		g.agregarArista(1, 0, 4);
		g.agregarArista(1, 3, 14);
		g.agregarArista(2, 1, 9);
		g.agregarArista(3, 2, 7);
		
		
		System.out.println(g.flujoMax());

	}
}
